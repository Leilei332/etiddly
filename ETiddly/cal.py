import csv
import pyecharts.options as opts
from pyecharts.charts import Calendar

create = ()
modify = ()
cal = []
date = []
data = []
csv.field_size_limit(500 * 1024 * 1024)
csvfile = open(r"C:\Users\admin\Documents\twcom.csv", 'r', encoding='utf-8')  # twcom.csv from tiddlywiki.com
with csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        create += (row['created'],)
        modify += (row['modified'],)
calall = create + modify
for i in calall:
    if i == '':
        pass
    else:
        cal.append(i[0] + i[1] + i[2] + i[3] + '-' + i[4] + i[5] + '-' + i[6] + i[7])
for j in cal:
    if date.count(j) == 0:
        date.append(j)
        data.append(1)
    else:
        data[date.index(j)] += 1
bundle = zip(date, data)
bundle = list(bundle)
calendar = Calendar()
calendar.add(series_name='', yaxis_data=bundle, calendar_opts=opts.CalendarOpts(range_=['2021-01-01', '2022-01-01']))
calendar.set_global_opts(title_opts=opts.TitleOpts(title='TiddlyWiki Dates'),
                         visualmap_opts=opts.VisualMapOpts(max_=10, min_=0, orient="horizontal", is_piecewise=False))
calendar.render("cal.html")
