import csv
import pyecharts

block = ["$:/tags/SideBar"]  # block system tags
links = ()
tags = ()
num = ()
lst = []


def tag2list(a):
    ot = a.replace("]]", "[[")
    tglst = ot.split('[[')
    for i in tglst:
        if tglst.index(i) % 2 == 0:
            glst = i.split(' ')
            for j in glst:
                if j == '':
                    pass
                else:
                    lst.append(j)
        else:
            lst.append(i)


csv.field_size_limit(500 * 1024 * 1024)
csvfile = open(r"C:\Users\admin\Documents\twcom.csv", 'r', encoding='utf-8')  # twcom.csv from tiddlywiki.com
with csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        links += (row['tags'],)
for i in links:
    lst = []
    tag2list(i)
    for j in lst:
        if block.count(j) != 0:
            pass
        elif tags.count(j) == 0:
            tags += (j,)
            num += (1,)
        else:
            num[tags.index(j)] += 1
bundle = list(zip(tags, num))
cloud = pyecharts.charts.WordCloud()
cloud.add(series_name='2021 Tags', data_pair=bundle)
cloud.render('wordcloud.html')
