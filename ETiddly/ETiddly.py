import os
import csv
import sys
import pyecharts
import pyecharts.options as opts
from PySide2 import QtCore, QtGui, QtWidgets

csv.field_size_limit(500 * 1024 * 1024)

class Counter(QtCore.QThread):
    prog = QtCore.Signal(int)
    sig = QtCore.Signal()
    initsig = QtCore.Signal(int)

    def __init__(self, csvfile):
        super(Counter, self).__init__()
        self.csvfile = open(csvfile, 'r', encoding='utf-8')

    def tag2list(self, a):
        ot = a.replace("]]", "[[")
        tglst = ot.split('[[')
        for i in tglst:
            if tglst.index(i) % 2 == 0:
                glst = i.split(' ')
                for j in glst:
                    if j == '':
                        pass
                    else:
                        self.lst += (j,)
            else:
                self.lst += (i,)

    def run(self):
        self.lst = ()
        self.links = ()
        self.tags = ()
        self.num = []
        self.create = ()
        self.modify = ()
        self.cal = []
        self.date = []
        self.data = []
        self.lst = ()
        with self.csvfile:
            reader = csv.DictReader(self.csvfile)
            for row in reader:
                self.links += (row['tags'],)
                self.create += (row['created'],)
                self.modify += (row['modified'],)
        calall = self.create + self.modify
        pro = 0
        prog = len(calall + self.links)
        self.initsig.emit(prog)
        for i in calall:
            if i == '':
                pass
            else:
                self.cal.append(i[0] + i[1] + i[2] + i[3] + '-' + i[4] + i[5] + '-' + i[6] + i[7])
            pro += 1
            self.prog.emit(pro)
        for j in self.cal:
            if self.date.count(j) == 0:
                self.date.append(j)
                self.data.append(1)
            else:
                self.data[self.date.index(j)] += 1
        for k in self.links:
            self.tag2list(k)
            for j in self.lst:
                if self.tags.count(j) == 0:
                    self.tags += (j,)
                    self.num.append(1)
                else:
                    self.num[self.tags.index(j)] += 1
            pro += 1
            self.prog.emit(pro)
        self.cbundle = list(zip(self.date, self.data))
        self.wbundle = list(zip(self.tags, self.num))
        self.sig.emit()
        self.wait()


class ETiddlyWizard(QtWidgets.QWizard):
    def __init__(self):
        super(ETiddlyWizard, self).__init__()
        self.settings = QtCore.QSettings('etiddly.ini', QtCore.QSettings.IniFormat)
        if not os.path.exists('etiddly.ini'):
            self.settings.setValue('file', '')
            self.settings.setValue('output', '')
            self.settings.setValue('title', 'TiddlyWiki Statistics')
            self.settings.setValue('subtitle', '')
            self.settings.setValue('calendar/drange_1', QtCore.QDate(QtCore.QDate.currentDate().year(), 1, 1))
            self.settings.setValue('calendar/drange_2', QtCore.QDate(QtCore.QDate.currentDate().year() + 1, 1, 1))
            self.settings.setValue('calendar/heatmap', 10)
            self.settings.setValue('calendar/piecewise', 0)
            self.settings.setValue('wordcloud/shape', 0)
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Wizard')
        if sys.platform == 'win32':
            self.setWizardStyle(QtWidgets.QWizard.AeroStyle)
        else:
            self.setWizardStyle(QtWidgets.QWizard.ModernStyle)
        self.page1 = QtWidgets.QWizardPage()
        self.page1.setTitle("Select File")
        self.page1.setSubTitle("In the page $:/AdvancedSearch, you can export tiddlywiki into csv file.")
        page1grid = QtWidgets.QGridLayout()
        page1grid.addWidget(QtWidgets.QLabel('File:'), 0, 0)
        page1grid.addWidget(QtWidgets.QLabel('Title:'), 2, 0)
        page1grid.addWidget(QtWidgets.QLabel('Subtitle:'), 3, 0)
        self.etitle = QtWidgets.QLineEdit()
        self.etitle.setPlaceholderText('TiddlyWiki Statistics')
        self.etitle.setText(self.settings.value('title'))
        page1grid.addWidget(self.etitle, 2, 1)
        self.esub = QtWidgets.QLineEdit()
        self.esub.setText(self.settings.value('subtitle'))
        page1grid.addWidget(self.esub, 3, 1)
        hbox = QtWidgets.QHBoxLayout()
        self.fedit = QtWidgets.QLineEdit()
        hbox.addWidget(self.fedit)
        fselect = QtWidgets.QPushButton('...')
        fselect.clicked.connect(self.fileSelect)
        hbox.addWidget(fselect)
        page1grid.addLayout(hbox, 0, 1)
        page1grid.addWidget(QtWidgets.QLabel('Output HTML:'), 1, 0)
        hbox2 = QtWidgets.QHBoxLayout()
        self.oedit = QtWidgets.QLineEdit()
        hbox2.addWidget(self.oedit)
        oselect = QtWidgets.QPushButton('...')
        oselect.clicked.connect(self.saveFile)
        hbox2.addWidget(oselect)
        page1grid.addLayout(hbox2, 1, 1)
        self.page1.setLayout(page1grid)
        self.page1.registerField('file*', self.fedit)
        self.page1.registerField('output*', self.oedit)
        self.page1.registerField('title', self.etitle)
        self.page1.registerField('subtitle', self.esub)
        self.addPage(self.page1)
        self.fedit.setText(self.settings.value('file'))
        self.oedit.setText(self.settings.value('output'))
        self.page2 = QtWidgets.QWizardPage()
        self.page2.setTitle('Config Chart')
        self.page2.setSubTitle('Configure chart options.')
        page2vbox = QtWidgets.QVBoxLayout()
        cgroup = QtWidgets.QGroupBox('Calendar')
        cgrid = QtWidgets.QGridLayout()
        cgrid.addWidget(QtWidgets.QLabel('Heatmap Range:'), 1, 0)
        self.hrange = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.hrange.setRange(10, 50)
        hlabel = QtWidgets.QLabel("0-")
        hspin = QtWidgets.QSpinBox()
        hspin.setRange(10, 50)
        self.hrange.valueChanged.connect(lambda: hspin.setValue(self.hrange.value()))
        hspin.valueChanged.connect(lambda: self.hrange.setValue(hspin.value()))
        self.hrange.setValue(int(self.settings.value('calendar/heatmap')))
        hspin.setValue(int(self.settings.value('calendar/heatmap')))
        hbox3 = QtWidgets.QHBoxLayout()
        hbox3.addWidget(hlabel)
        hbox3.addWidget(hspin)
        hbox3.addWidget(self.hrange)
        cgrid.addLayout(hbox3, 1, 1)
        cgrid.addWidget(QtWidgets.QLabel('Date Range:'), 0, 0)
        self.drange_1 = QtWidgets.QDateEdit()
        self.drange_2 = QtWidgets.QDateEdit()
        self.drange_1.setCalendarPopup(True)
        self.drange_2.setCalendarPopup(True)
        self.drange_1.setDate(self.settings.value('calendar/drange_1'))
        self.drange_2.setDate(self.settings.value('calendar/drange_2'))
        hbox4 = QtWidgets.QHBoxLayout()
        hbox4.addWidget(self.drange_1)
        hbox4.addWidget(self.drange_2)
        cgrid.addLayout(hbox4, 0, 1)
        self.pw = QtWidgets.QCheckBox("Piecewise")
        self.pw.setChecked(bool(int(self.settings.value('calendar/piecewise'))))
        cgrid.addWidget(self.pw, 2, 0, 1, 2)
        cgroup.setLayout(cgrid)
        page2vbox.addWidget(cgroup)
        wgroup = QtWidgets.QGroupBox('Wordcloud')
        wgrid = QtWidgets.QGridLayout()
        wgrid.addWidget(QtWidgets.QLabel('Shape:'), 0, 0)
        self.wshape = QtWidgets.QComboBox()
        self.wshape.addItems(['circle', 'cardioid', 'diamond', 'triangle-forward', 'triangle', 'pentagon', 'star'])
        self.wshape.setCurrentIndex(int(self.settings.value('wordcloud/shape')))
        wgrid.addWidget(self.wshape, 0, 1)
        wgroup.setLayout(wgrid)
        page2vbox.addWidget(wgroup)
        self.page2.setLayout(page2vbox)
        self.addPage(self.page2)
        btn = self.button(QtWidgets.QWizard.FinishButton)
        btn.clicked.connect(self.addData)
        self.show()

    def fileSelect(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, "Open", "tiddlers.csv", "CSV Files (*.csv)")
        self.fedit.setText(fname[0])
        self.oedit.setText(fname[0].replace('.csv', '.html'))

    def saveFile(self):
        sname = QtWidgets.QFileDialog.getSaveFileName(self, "Save", "tiddlers.html", "HTML File (*.html)")
        self.oedit.setText(sname[0])

    def addData(self):
        self.count = Counter(csvfile=self.fedit.text())
        self.count.prog.connect(self.updUI)
        self.count.initsig.connect(self.pInit)
        self.count.sig.connect(self.genHTML)
        self.progress = QtWidgets.QProgressDialog('Processing...', 'Cancel', 0, 0)
        self.progress.setWindowTitle('Processing...')
        self.progress.setCancelButton(None)
        self.count.start()
        self.progress.show()

    def pInit(self, n):
        self.progress.setRange(0, n)

    def updUI(self, n):
        self.progress.setValue(n)

    def genHTML(self):
        self.count.terminate()
        self.progress.setRange(0, 0)
        cale = pyecharts.charts.Calendar()
        cale.add(series_name='Statistics', yaxis_data=self.count.cbundle,
                 calendar_opts=opts.CalendarOpts(range_=[self.drange_1.date().toString(QtCore.Qt.ISODate),
                                                         self.drange_2.date().toString(QtCore.Qt.ISODate)]))
        cale.set_global_opts(title_opts=opts.TitleOpts(title=self.etitle.text(), subtitle=self.esub.text()),
                             visualmap_opts=opts.VisualMapOpts(max_=self.hrange.value(), min_=0, orient="horizontal",
                                                               is_piecewise=self.pw.isChecked()))
        cloud = pyecharts.charts.WordCloud()
        cloud.add(series_name='Statistics', data_pair=self.count.wbundle, shape=self.wshape.currentText())
        tab = pyecharts.charts.Tab()
        tab.add(cale, 'calendar')
        tab.add(cloud, 'wordcloud')
        tab.render(self.oedit.text())
        self.settings.setValue('file', self.fedit.text())
        self.settings.setValue('output', self.oedit.text())
        self.settings.setValue('title', self.etitle.text())
        self.settings.setValue('subtitle', self.esub.text())
        self.settings.setValue('calendar/drange_1', self.drange_1.date())
        self.settings.setValue('calendar/drange_2', self.drange_2.date())
        self.settings.setValue('calendar/heatmap', self.hrange.value())
        self.settings.setValue('calendar/piecewise', int(self.pw.isChecked()))
        self.settings.setValue('wordcloud/shape', self.wshape.currentIndex())
        QtWidgets.QMessageBox.information(self, 'Complete',
                                          "Complete Generating HTML. <a href='file://{0}'>View</a>".format(
                                              self.oedit.text()))
        sys.exit()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    eti = ETiddlyWizard()
    sys.exit(app.exec_())
