from cx_Freeze import setup, Executable
import sys

build_exe_options = {"includes": ['PySide2.QtWidgets', 'PySide2.QtCore', 'PySide2.QtGui'], 'packages': ['pyecharts'],
                     'excludes': ['tkinter', 'lib2to3', 'ftplib', 'cx-Freeze', 'cx-Logging'],
                     'bin_path_excludes': ['ETiddly/etiddly.ini']}
base = None
if sys.platform == "win32":
    base = "Win32GUI"
setup(
    name='ETiddly',
    version='0.1.1',
    description='Generate statistics for TiddlyWiki',
    options={'build_exe': build_exe_options},
    executables=[Executable('ETiddly/ETiddly.py', base=base, icon='ETiddly/app-icon.ico', shortcut_name='ETiddly', shortcut_dir='ETiddly')]
)
